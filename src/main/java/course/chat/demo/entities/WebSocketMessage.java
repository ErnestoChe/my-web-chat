package course.chat.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

/**
 * @author erph0511
 */
@Data
@With
@AllArgsConstructor
@NoArgsConstructor
public class WebSocketMessage {

    private String type;
    private String content;
    private String sender;

}
